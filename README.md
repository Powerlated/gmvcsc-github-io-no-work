# Website
Welcome to the Greater Miami Valley Computer Science Circle website. The intent of this repository is to allow students and faculty to develop and publish web content simply using Github pages.

This website is simple html, css and javascript so you should be able to edit it with any editor and view the full site in your browser locally.

## Developing
To develop website content, you fork this repository, make changes and then issue a pull request back. Pull requests will be reviewed for security and content. If changes are required, they can simply be added to the existing pull request. Please remember to squash commits to minimize fluff in the commit logs.

## How to add a page
To add a page, go into `templates/nav.html` and add an entry for the page for it to appear in the navbar. After that, add a corresponding page in `contents`.